public class LinksChecker {

    /**
     * Le type d'exception retourné par les méthodes de cette classe.
     */
    public class Exception extends java.lang.Exception {
        /**
         * l'un des messages suivants, selon le problème :
         * - "URL malformée"
         * - "URL non-HTTP"
         * - "erreur HTTP xxx" (suivi du code à 3 chiffres)
         */
        public String message;
        /**
         * l'URL qui a posé problème
         */
        public String url;

        public Exception(String message, String url) {
            this.message = message;
            this.url = url;
        }
    }

    /**
     * Charge la page à l'URL donnée, et vérifie tous ses liens
     * en comptant les liens corrects, brisés et non-HTTP.
     */
    public void check(String urlstr) throws LinksChecker.Exception {
        // TODO
    }

    /**
     * Retourne le nombre de liens OK vérifiés jusqu'à maintenant
     */
    public int linksOk() {
        return linksOk;
    }

    /**
     * Retourne le nombre de liens brisés vérifiés jusqu'à maintenant
     */
    public int linksKo() {
        return linksKo;
    }

    /**
     * Retourne le nombre de liens non-HTTP vérifiés jusqu'à maintenant
     */
    public int linksUnknown() {
        return linksUnknown;
    }

    int linksOk = 0;
    int linksKo = 0;
    int linksUnknown = 0;

    // Vous êtes bien sûr libres de rajouter
    // des méthodes et attributs privés à cette classe.
}
